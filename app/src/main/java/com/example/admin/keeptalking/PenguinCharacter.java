package com.example.admin.keeptalking;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by admin on 11.09.2017.
 */
public class PenguinCharacter extends GameObject{

    private static final int X_AXE = 20;
    public static final float VELOCITY = 0.5f;
    private Bitmap image;

    private int movingVectorY = 1;
    private long lastDrawNanoTime =-1;
    private GameSurface gameSurface;

    public PenguinCharacter(GameSurface gameSurface, Bitmap image, int y) {
        super(image,X_AXE, y);
        this.gameSurface= gameSurface;
        this.image = image;
    }

    public void update()  {
        long now = System.nanoTime();

        if(lastDrawNanoTime==-1) {
            lastDrawNanoTime= now;
        }
        int deltaTime = (int) ((now - lastDrawNanoTime)/ 1000000);

        float distance = VELOCITY * deltaTime;

        double movingVectorLength = Math.sqrt(movingVectorY*movingVectorY);

        this.y = y +  (int)(distance* movingVectorY / movingVectorLength);

        if(this.y < 0 )  {
            this.y = 0;
            this.movingVectorY = - this.movingVectorY;
        } else if(this.y > this.gameSurface.getHeight()- height)  {
            this.y= this.gameSurface.getHeight()- height;
            this.movingVectorY = - this.movingVectorY ;
        }

    }

    public void draw(Canvas canvas)  {
        Bitmap image = this.image;
        canvas.drawBitmap(image,X_AXE, y, null);
        this.lastDrawNanoTime= System.nanoTime();
    }
}
