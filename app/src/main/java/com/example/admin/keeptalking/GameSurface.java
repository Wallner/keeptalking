package com.example.admin.keeptalking;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by admin on 11.09.2017.
 */
public class GameSurface extends SurfaceView implements SurfaceHolder.Callback {


    private GameThread gameThread;

    private PenguinCharacter pingu1;
    private RockObstacle rock1;

    public GameSurface(Context context)  {
        super(context);

        this.setFocusable(true);
        this.getHolder().addCallback(this);
    }

    public void update()  {
        this.pingu1.update();
        this.rock1.update();
    }

    @Override
    public void draw(Canvas canvas)  {
        super.draw(canvas);
            this.pingu1.draw(canvas);
            this.rock1.draw(canvas);

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Bitmap PenguinBitmap1 = BitmapFactory.decodeResource(this.getResources(),R.drawable.pingu);
        this.pingu1 = new PenguinCharacter(this,PenguinBitmap1,50);

        Bitmap rockBitmap1 = BitmapFactory.decodeResource(this.getResources(),R.drawable.rock_down1);
        this.rock1 = new RockObstacle(this,rockBitmap1,1000, 0);

        this.gameThread = new GameThread(this,holder);
        this.gameThread.setRunning(true);
        this.gameThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry= true;
        while(retry) {
            try {
                this.gameThread.setRunning(false);

                this.gameThread.join();
            }catch(InterruptedException e)  {
                e.printStackTrace();
            }
            retry= true;
        }
    }

}
